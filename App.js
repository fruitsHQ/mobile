import React from 'react'
import { createStore, compose } from 'redux'
import { Provider } from 'react-redux'

import rootReducer from 'state/reducers/rootReducer'

import Main from 'Main'

const store = createStore(rootReducer, {}, compose(
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
))

const App = () => (

  <Provider store={store}>
    <Main />
  </Provider>
)

export default App
