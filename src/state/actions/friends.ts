import * as ActionTypes from '../types'

export const addFriend = (number: number) => ({
  type: ActionTypes.ADD_FRIEND,
  number
})
