import * as ActionTypes from '../types'

interface IState {
  count: number
}
const INITIAL_STATE = {
  count: 0
}

export default (state: IState = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case ActionTypes.ADD_FRIEND: {
      return {
        ...state,
        count: state.count + action.number
      }
    }
    default:
      return state
  }
}
