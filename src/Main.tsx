// eslint-disable-next-line no-use-before-define
import React from 'react'
import { NativeRouter, Route } from 'react-router-native'
import { Root } from 'native-base'

import ProfileScreen from 'profile'
import TranslatorScreen from 'translator'
import DictionaryScreen from 'dictionary'
import TrainingScreen from 'training'
import Footer from 'shared/Footer'
import Header from 'shared/Header'

const Main: React.FC = () => (
  <Root>
    <NativeRouter>
      <Header />
      <Route exact path="/" component={TranslatorScreen} />
      <Route path="/profile" component={ProfileScreen} />
      <Route path="/translator" component={TranslatorScreen} />
      <Route path="/dictionary" component={DictionaryScreen} />
      <Route path="/training" component={TrainingScreen} />
      <Footer />
    </NativeRouter>
  </Root>

)

export default Main
