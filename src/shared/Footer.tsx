// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Button, Container, Content, Footer, FooterTab, Header, Icon, Text
} from 'native-base'

import { Link } from 'react-router-native'

const FooterTabsIconTextExample = () => (
  <Container>
    <Header />
    <Content />
    <Footer>
      <FooterTab>
        <Button vertical>
          <Link to="profile">
            <Icon name="person" />
          </Link>
          <Text>Profile</Text>
        </Button>
        <Button vertical>
          <Link to="translator">
            <Icon name="camera" />
          </Link>
          <Text>Translate</Text>
        </Button>
        <Button vertical active>
          <Link to="dictionary">
            <Icon active name="navigate" />
          </Link>
          <Text>Dictionary</Text>
        </Button>
        <Button vertical>
          <Link to="training">
            <Icon name="person" />
          </Link>
          <Text>Training</Text>
        </Button>
      </FooterTab>
    </Footer>
  </Container>
)

export default FooterTabsIconTextExample
